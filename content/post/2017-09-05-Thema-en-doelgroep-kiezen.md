---
title: Thema en doelgroep kiezen
date: 2017-09-05
---
Met het team hebben we vandaag het thema en de doelgroep gekozen, we vonden allemaal architectuur interessant en dachten daarom aan bouwkunde studenten. We kwamen erachter dat bouwkunde studenten eigenlijk heel anders is dan architectuur, maar besloten toch bouwkunde studenten als doelgroep te houden. Uiteindelijk kozen we voor constructies, omdat we daar veel kanten mee op konden. We maakten allemaal moodboards over constructies. 